provider "aws" {}


variable "cidr_blocks" {
    description = "subnet cidr blocks and name tags for vpc and subnets"
    type = list(object({
        cidr_block = string
        name = string
    }))
}

variable avail_zone {}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        Name: var.cidr_blocks[0].name
        vpc_env: "dev"
    }
    }

resource "aws_subnet" "dev-subnet-1" {
        vpc_id = aws_vpc.development-vpc.id
        cidr_block = var.cidr_blocks[1].cidr_block
        availability_zone = var.avail_zone
        tags = {
          Name = var.cidr_blocks[1].name
        }
}


data "aws_vpc" "existing_vpc" {
    default = true 
}